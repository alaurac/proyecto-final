import java.net.ServerSocket;
import java.net.Socket;

public class NodoServer extends Thread{
	
	@Override
	public void run() {
		super.run();
		
		try{
			ServerSocket server = new ServerSocket(5000);
			
			while ( true ) {
				Socket coneccion = server.accept();
				
				new ConeccionNodoNodo(coneccion).start();
			}
		}catch (Exception e ) {
			System.err.println(e);
		}
	}
	
}
