import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.net.Socket;

//esta clase solo se encarga de enviar un archivo cuando se le solicita
public class ConeccionNodoNodo extends Thread{
	Socket coneccion;
	DataInputStream lector;
	DataOutputStream escritor;
	
	public ConeccionNodoNodo(Socket coneccion) throws Exception {
		this.coneccion = coneccion;
		lector = new DataInputStream(coneccion.getInputStream());
		escritor = new DataOutputStream(coneccion.getOutputStream());
	}
	
	@Override
	public void run() {
		super.run();
		
		try {
			String entrada = lector.readUTF();
			
			new Transferencia(coneccion).enviar(new File(entrada));
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
