import java.net.ServerSocket;
import java.net.Socket;

//clase terminada 
enum Operacion{
	INICIAR_SESION,
	SESION_EXITOSA,
	SESION_FALLIDA,
	CERRAR_SESION,
	AGREGAR_ARCHIVO,
	SOLICITAR_ARCHIVOS
}
public class Master {
	
	public static void main (String[] args){
		try{
			ServerSocket server = new ServerSocket(5000);
			
			while ( true ) {
				Socket coneccion = server.accept();
				
				new ConeccionMasterNodo(coneccion).start();
			}
		}catch (Exception e ) {
			System.err.println(e);
		}
	}
}
