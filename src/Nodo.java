
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.net.Socket;
import java.util.Scanner;

public class Nodo {
	public static void main(String[] args) {
		NodoServer nodoServer = new NodoServer();
		nodoServer.start();
		
		String ip = "";
		if(args.length == 1) {
			ip = args[0];
		}else {
			System.err.println("java Cliente ipMaster");
			System.exit(0);
		}
		try{
			Socket servidor = new Socket(ip, 5000);
			DataInputStream lector = new DataInputStream(servidor.getInputStream());
			DataOutputStream escritor = new DataOutputStream(servidor.getOutputStream());
			Scanner eTeclado = new Scanner(System.in);
			while(true) {
				System.out.println("nombre de usuario:");
				String nombre = eTeclado.nextLine();
				
				System.out.println("contraseña:");
				String contraseña = eTeclado.nextLine();
				
				escritor.writeUTF(Operacion.INICIAR_SESION + "," + nombre + "," + contraseña);
				
				//esperar que el servidor confirme la sesion
				System.out.println("Iniciando sesion...");
				Operacion op = Operacion.valueOf(lector.readUTF());
				
				if(op == Operacion.SESION_FALLIDA) {
					System.out.println("nombre o contraseña incorrectas");
					continue;
				}
				
				System.out.println("Sesion iniciada");
				escritor.writeUTF(Operacion.SOLICITAR_ARCHIVOS.toString());
				String[] ipsArchivos = lector.readUTF().split(",");
				System.out.println("Actualizando archivos");
				//se envia una solicitud a cada nodo por una copia del archivo
				for(int i = 0 ; i < ipsArchivos.length ; i = i + 2) {
					new Transferencia(ipsArchivos[i]).recibir(new File(ipsArchivos[i + 1]));
				}
				
				System.out.println("Archivos actualizados");
				System.out.println("Ya puede trabajar con sus archivos");
				
				
				while(true) {
					String orden = eTeclado.nextLine();
					
					if(orden.equals("exit")) {
						System.out.println("Sesion cerrada");
						break;
					}else if(orden.startsWith("addfile")) {
						while(orden.contains("  ")) {
							orden = orden.replace("  ", " ");
						}
						String[] argumentos = orden.split(" ");
						if(argumentos.length != 2) {
							System.out.println("Debe ingresar un nombre de archivo");
							continue;
						}
						File f = new File(argumentos[1]);
						if(f.exists() && !f.isDirectory()) {
							escritor.writeUTF(Operacion.AGREGAR_ARCHIVO + "," + argumentos[1]);
						}else {
							System.out.println("El archivo a guardar no existe");
						}
					}else {
						System.out.println("comando no valido");
						System.out.println("pruebe con exit o addfile nombredearchivo");
					}
				}
			}
		}catch ( Exception e ) {
			System.err.println(e);
		}
	}
}
