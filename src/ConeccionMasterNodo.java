
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

import beans.Archivo;
import beans.Usuario;
import conexion.Operaciones;

public class ConeccionMasterNodo extends Thread{
	Socket coneccion;
	DataInputStream lector;
	DataOutputStream escritor;
	
	public ConeccionMasterNodo(Socket coneccion) throws Exception {
		this.coneccion = coneccion;
		lector = new DataInputStream(coneccion.getInputStream());
		escritor = new DataOutputStream(coneccion.getOutputStream());
	}
	
	@Override
	public void run() {
		super.run();
		
		try {
			Operaciones ops = new Operaciones();
			String nombre="";
			while(true) {
				//op,nombre,contraseña
				String[] entradas = lector.readUTF().split(",");
				System.out.println("parametro: " + entradas[0]);
				Operacion op = Operacion.valueOf(entradas[0]);
				

				if(op == Operacion.INICIAR_SESION) {
					//verificar datos para inicio de sesion
					nombre = entradas[1];
					String contrasena = entradas[2];
					boolean sesion_iniciada = ops.iniciarSesion(nombre, contrasena);
					if(sesion_iniciada) {
						escritor.writeUTF(Operacion.SESION_EXITOSA.toString());
					}else {
						escritor.writeUTF(Operacion.SESION_FALLIDA.toString());
					}
				}else if(op == Operacion.AGREGAR_ARCHIVO) {
					//op,ruta
					String ruta = entradas[1];
					String ip = coneccion.getRemoteSocketAddress().toString();
					//incluir archivo a la base de datos
					ops.agregarArchivo(nombre, ip, ruta); 
					
					
				}else if(op.equals(Operacion.SOLICITAR_ARCHIVOS)) {
					System.out.println("me estan solicitando algo");
					//mando a los nodos la lista de nodos donde se encuentran todos sus archivos(aqui)
					System.out.println("nombre = " + nombre + " ops = " + ops.obtenerArchivos(nombre));
					escritor.writeUTF(ops.obtenerArchivos(nombre));
				}				
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
