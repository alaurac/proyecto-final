package  conexion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.text.BadLocationException;

import conexion.Conexion;
import beans.Archivo;
import beans.Usuario;
public class Operaciones {

    public void agregarUsuario(Usuario user) throws SQLException, BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        Statement sentencia = bd.conex().createStatement();

        query = "INSERT INTO usuario(userUsuario, " + "passUsuario " + ") VALUES ('" + user.getUsername() 
        		+ "', " + "'" + user.getContrasena() + "');";
        System.out.println(query);
        if (sentencia.executeUpdate(query) > 0) {
            System.out.println("El registro se insert� exitosamente.");
        } else {
            System.out.println("No se pudo insertar el registro.");
        }

        //System.out.println(query);
        sentencia.close();
        bd.con.close();
    }

    public void editarUsuarioContra(Usuario user, String nuevaContra) throws BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        try {
            query = "UPDATE usuario SET passUsuario = ? WHERE idUsuario = ?;";
            
            PreparedStatement sentenciaP = bd.conex().prepareStatement(query);
            sentenciaP.setString(1, nuevaContra);
            sentenciaP.setInt(2, user.getIdUser());
            System.out.println(query);
            sentenciaP.executeUpdate();
            System.out.println("El registro se actualiz� exitosamente.");
            sentenciaP.close();
            bd.con.close();
            //return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            //return false;
        }
    }

    public void eliminar(Usuario user) throws BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        try {
            query = "UPDATE usuario SET estadoUsuario = 1 WHERE idUsuario = ?;";
            PreparedStatement sentenciaP = bd.conex().prepareStatement(query);
            
            sentenciaP.setInt(1, user.getIdUser());

            sentenciaP.executeUpdate();
            System.out.println("El registro se actualiz� exitosamente.");
            sentenciaP.close();
            bd.con.close();
            //return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            //return false;
        }
    }

    public void obtener(Usuario user) throws BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        try {
            query = "SELECT * FROM usuario WHERE idUsuario = '" + user.getIdUser() + "';";
            Statement sentencia = bd.conex().createStatement();
            ResultSet resultado = sentencia.executeQuery(query);

            while (resultado.next()) {
                int id = resultado.getInt("idUsuario");
                String username = resultado.getString("userUsuario");
                String contrasena = resultado.getString("passUsuario");

                // Imprimir los resultados.
                System.out.format("%d, %s, %s, \n", id, username, contrasena);
            }

            sentencia.close();
            bd.con.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    //lo mismo de arriba, pero solo por ID
    public void obtener(int id) throws BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        try {
            query = "SELECT * FROM usuario WHERE idUsuario = '" + id + "';";
            Statement sentencia = bd.conex().createStatement();
            ResultSet resultado = sentencia.executeQuery(query);

            while (resultado.next()) {
                int ide = resultado.getInt("idUsuario");
                String username = resultado.getString("userUsuario");
                String contrasena = resultado.getString("passUsuario");
                System.out.format("%d, %s, %s, \n", ide, username, contrasena);
            }
            sentencia.close();
            bd.con.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public int obtenerIdUsuario(String username) throws BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        int ide=0;
        try {
            query = "SELECT idUsuario FROM usuario WHERE userUsuario = '" + username + "';";
            //System.out.println("quee"+query);
            Statement sentencia = bd.conex().createStatement();
            ResultSet resultado = sentencia.executeQuery(query);

            while (resultado.next()) {
                ide = resultado.getInt("idUsuario");
            }
            sentencia.close();
            bd.con.close();
            return ide;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }
    public boolean iniciarSesion(Usuario user) throws BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        try {
            query = "SELECT * FROM usuario WHERE userUsuario = '" + user.getUsername() + "';";
            Statement sentencia = bd.conex().createStatement();
            ResultSet resultado = sentencia.executeQuery(query);
            if (resultado.absolute(1)) {
            	if(resultado.getString("passUsuario").equals(user.getContrasena())) {
            		System.out.println("inicio de sesion existosa");
            	}else {
            		System.out.println("contrase�a incorrecta");
            	}
            }else {
            	System.out.println("no existe usuario");
            }
            sentencia.close();
            bd.con.close();
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
    public boolean iniciarSesion(String user,String contrasena) throws BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        try {
            query = "SELECT * FROM usuario WHERE userUsuario = '" + user + "';";
            Statement sentencia = bd.conex().createStatement();
            ResultSet resultado = sentencia.executeQuery(query);
            if (resultado.absolute(1)) {
            	if(resultado.getString("passUsuario").equals(contrasena)) {
            		System.out.println("inicio de sesion existosa");
            		return true;
            	}else {
            		System.out.println("contrase�a incorrecta");
            	}
            }else {
            	System.out.println("no existe usuario");
            }
            sentencia.close();
            bd.con.close();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            
        }
        return false;
    }
    public void agregarArchivo(Usuario user,Archivo arch) throws SQLException, BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        Statement sentencia = bd.conex().createStatement();

        query = "INSERT INTO archivo(idUsuario, " + "ipArchivo, " + "rutaArchivo " + ") VALUES ('" + user.getIdUser() 
        		+ "', " + "'" + arch.getIp() + "', " + "'" + arch.getRuta() + "');";
        //System.out.println(query);
        if (sentencia.executeUpdate(query) > 0) {
            System.out.println("El registro se insert� exitosamente.");
        } else {
            System.out.println("No se pudo insertar el registro.");
        }
        sentencia.close();
        bd.con.close();
    }
    public void agregarArchivo(int idUser,String ip,String rutaArch) throws SQLException, BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        Statement sentencia = bd.conex().createStatement();

        query = "INSERT INTO archivo(idUsuario, " + "ipArchivo, " + "rutaArchivo " + ") VALUES ('" + idUser 
		+ "', " + "'" + ip + "', " + "'" + rutaArch + "');";
        //System.out.println(query);
        if (sentencia.executeUpdate(query) > 0) {
            System.out.println("El registro se insert� exitosamente.");
        } else {
            System.out.println("No se pudo insertar el registro.");
        }
        sentencia.close();
        bd.con.close();
    }
    public void agregarArchivo(Usuario user,String ip,String rutaArch) throws SQLException, BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        Statement sentencia = bd.conex().createStatement();

        query = "INSERT INTO archivo(idUsuario, " + "ipArchivo, " + "rutaArchivo " + ") VALUES ('" + user.getIdUser()
		+ "', " + "'" + ip + "', " + "'" + rutaArch + "');";
        //System.out.println(query);
        if (sentencia.executeUpdate(query) > 0) {
            System.out.println("El registro se insert� exitosamente.");
        } else {
            System.out.println("No se pudo insertar el registro.");
        }
        sentencia.close();
        bd.con.close();
    }
    public void agregarArchivo(String username,String ip, String rutaArch) throws SQLException, BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        Statement sentencia = bd.conex().createStatement();
        int idUsuario =obtenerIdUsuario(username);
        

        query = "INSERT INTO archivo(idUsuario, " + "ipArchivo ," + "rutaArchivo " + ") VALUES ('" + idUsuario 
        		+ "', " + "'" + ip + "', " + "'" + rutaArch + "');";
        //System.out.println(query);
        if (sentencia.executeUpdate(query) > 0) {
            System.out.println("El registro se insert� exitosamente.");
        } else {
            System.out.println("No se pudo insertar el registro.");
        }
        sentencia.close();
        bd.con.close();
    }
    public String[] obtenerArchivos(Usuario user) throws BadLocationException {
        String query = "";
        Conexion bd = new Conexion();
        String[]archivos;
        try {
            query = "SELECT * FROM archivo WHERE idUsuario = '" + user.getIdUser() + "';";
            Statement sentencia = bd.conex().createStatement();
            ResultSet resultado = sentencia.executeQuery(query);
            archivos=new String[resultado.getFetchSize()];
            int iterador=0;
            while (resultado.next()) {
                int id = resultado.getInt("idUsuario");
                String ip = resultado.getString("ipArchivo");
                String ruta = resultado.getString("rutaArchivo");
                archivos[iterador]=ip;
                archivos[iterador+1]=ruta;
                // Imprimir los resultados.
                System.out.format("%d, %s, %s, \n", id, ip, ruta);
                iterador=iterador+2;
            }
            sentencia.close();
            bd.con.close();
            return archivos;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    public String obtenerArchivos(String username) throws BadLocationException {
        String prQuery="";
        String query = "";
        Conexion bd = new Conexion();
        String archivos="";
        int idUsuario=0;
        	try {
        		prQuery="select * from usuario where userUsuario = '"+username+"';";
        		Statement sentencia = bd.conex().createStatement();
                ResultSet resultado = sentencia.executeQuery(prQuery);
                while (resultado.next()) {
                	idUsuario = resultado.getInt("idUsuario");
                }
                //System.out.println(prQuery+"id="+idUsuario);
        	}catch(Exception e) {
        		System.out.println("csm"+e.getMessage());
        	}
        	try {
                query = "SELECT * FROM archivo WHERE idUsuario = '" + idUsuario + "';";
                Statement sentencia1 = bd.conex().createStatement();
                ResultSet resultado1 = sentencia1.executeQuery(query);
                
                int iterador=0;
                //System.out.println(query+"id="+idUsuario);
                while (resultado1.next()) {
                    int id = resultado1.getInt("idUsuario");
                    String ip = resultado1.getString("ipArchivo");
                    String ruta = resultado1.getString("rutaArchivo");
                    archivos+=ip+",";
                    archivos+=ruta+",";
                    // Imprimir los resultados.
                    //System.out.format("%d, %s, %s, \n", id, ip, ruta);
                    iterador=iterador+2;
                }
                sentencia1.close();
                bd.con.close();
                return archivos.substring(0,archivos.length()-1);
            } catch (Exception e) {
                System.out.println(e.getMessage()+"prro");
            }
        return "";
    }
}