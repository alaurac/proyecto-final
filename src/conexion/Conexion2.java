package conexion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion2 {

    // Librer�a de MySQL
    public static String driver = "com.mysql.jdbc.Driver";

    // Nombre de la base de datos
    public static String database = "archivos";

    // Host
    public static String hostname = "localhost";

    // Puerto
    public static String port = "3306";

    // Ruta de nuestra base de datos (desactivamos el uso de SSL con "?useSSL=false")
    public static String url = "jdbc:mysql://" + hostname + ":" + port + "/" + database + "?useSSL=false";

    // Nombre de usuario
    public static String username = "root";

    // Clave de usuario
    public static String password = "";
    static Connection conn = null;
    public static Connection conectarMySQL() {
        

        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
            System.out.println("good");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println("bad conection");
        }

        return conn;
    }
    public static void main(String[] args) {
    	conectarMySQL();
	}

}