package beans;

public class Archivo {

	public int idUsuario;
	public String ip;
	public String ruta;
	
	
	public Archivo(int idUsuario, String ip,String ruta) {
		super();
		this.ip=ip;
		this.idUsuario = idUsuario;
		this.ruta = ruta;
	}
	public Archivo( String ruta) {
		this.ruta = ruta;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ruta) {
		this.ip = ruta;
	}
	
}
