package beans;

public class Usuario {

	public int idUser;
	public String username;
	public String contrasena;
	
	
	public Usuario(int idUser, String username, String contrasena) {
		super();
		this.idUser = idUser;
		this.username = username;
		this.contrasena = contrasena;
	}
	public Usuario(String username, String contrasena) {
		
		this.username = username;
		this.contrasena = contrasena;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	 
}
