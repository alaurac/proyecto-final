import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Random;

//clase terminada
public class Transferencia {
	BufferedInputStream bis;
	BufferedOutputStream bos;
	Socket client;
	
	public Transferencia(Socket client) {
		this.client = client;
	}
	public Transferencia(String ip) {
		try {
			client = new Socket(ip, 5000);
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void enviar(File localFile) throws IOException, InterruptedException{
		byte[] byteArray = new byte[8192];
		int in;
		
		bis = new BufferedInputStream(new FileInputStream(localFile));
		bos = new BufferedOutputStream(client.getOutputStream());
		
		while ((in = bis.read(byteArray)) != -1){
			Thread.sleep(10);
			bos.write(byteArray,0,in);
		}
		bis.close();
		bos.close();
	}
	
	public void recibir(File localFile) throws IOException{
		//aqui le digo al nodo vecino cual es el nombre del archivo que quiero
		new DataOutputStream(client.getOutputStream()).writeUTF(localFile.getName());
		
		byte[] byteArray = new byte[1024];
		int in;
		
		bis = new BufferedInputStream(client.getInputStream());
		bos = new BufferedOutputStream(new FileOutputStream(localFile.getName() + new Random().nextInt(1000)));
		
		while ((in = bis.read(byteArray)) != -1){
			bos.write(byteArray,0,in);
		}
		bos.close();
		bis.close();
	}
}
